const TPLSmartDevice = require("tplink-lightbulb");
const jc = require("json-colorizer");
const coreAudio = require("node-core-audio");
const stream = require("stream");
const util = require("util");
const scan = TPLSmartDevice.scan();
const fs = require("fs");
const portAudio = require("naudiodon");
//const decode = require("audio-decode");
const decoder = require("wav-decoder");
const header = require("waveheader");
const convert = require("pcm-convert");
const AudioContext = require("web-audio-api").AudioContext;
context = new AudioContext;
const pcmdata = [];


function onBuffer(buffer) {
  context.decodeAudioData(buffer, function (audioBuffer) {
    console.log(audioBuffer.numberOfChannels, audioBuffer.length, audioBuffer.sampleRate, audioBuffer.duration);
    pcmdata = (audioBuffer.getChannelData(0));
    const samplerate = audioBuffer.sampleRate; // store sample rate
    const maxvals = [];
    const max = 0;
    //playsound(soundfile)
    findPeaks(pcmdata, samplerate);
  }, function (err) { throw err; });
}

function findPeaks(pcmdata, samplerate) {
  const interval = 0.05 * 1000;
  let index = 0;
  let step = Math.round( samplerate * (interval/1000) );
  let max = 0;
  let prevmax = 0;
  let prevdiffthreshold = 0.3;

  //loop through song in time with sample rate
  let samplesound = setInterval(function() {
    if (index >= pcmdata.length) {
      clearInterval(samplesound);
      console.log("finished sampling sound")
      return;
    }

    for(let i = index; i < index + step; i++){
      max = pcmdata[i] > max ? pcmdata[i].toFixed(1) : max;
    }

    // Spot a significant increase? Potential peak
    let bars = getbars(max) ;
    if (max-prevmax >= prevdiffthreshold) {
      bars += " == peak == ";
    }

    // Print out mini equalizer on commandline
    console.log(bars, max);
    prevmax = max;
    max = 0;
    index += step;
  }, interval, pcmdata);
}


function getbars(val) {
  let bars = "";
  for (var i = 0; i < val*50 + 2; i++) {
    bars += "|";
  }
  return bars;
}

function playsound(soundfile) {
    // linux or raspi
    // var create_audio = exec('aplay'+soundfile, {maxBuffer: 1024 * 500}, function (error, stdout, stderr) {
    const create_audio = exec('ffplay -autoexit '+soundfile, {maxBuffer: 1024 * 500}, function (error, stdout, stderr) {
    if (error !== null) {
      console.log('exec error: ' + error);
    } else {
      //console.log(" finshed ");
      //micInstance.resume();
    }
  });
}

const audioOptions = {
  channelCount: 2,
  sampleFormat: portAudio.SampleFormat16Bit,
  sampleRate: 44100,
  deviceId: -1
};

const fftSize = 64;

const Writable = stream.Writable;
const memStore = {};

function WMStrm(key, options) {
  if (!(this instanceof WMStrm)) {
    return new WMStrm(key, options);
  }
  Writable.call(this, options);
  this.key = key;
  memStore[key] = new Buffer("");
}
util.inherits(WMStrm, Writable);

WMStrm.prototype._write = function (chunk, enc, cb) {
  const buffer = (Buffer.isBuffer(chunk)) ? chunk : new Buffer(chunk, enc);
  //const json = buffer.toJSON();
  const float32arr = convert(buffer, "float32");
  //console.log("BUFFER LENGTH:", a, float32arr.length);

  memStore[this.key] = Buffer.concat([memStore[this.key], buffer]);
  //console.log("_write", chunk.toJSON(), enc);
  cb();
}

const wstream = new WMStrm("audio");

wstream.on("finish", function () {
  console.log("FINISHED", JSON.toString(memStore.audio));
  decoder.decode(memStore.audio).then(audioData => {
    console.log("audioData:", JSON.stringify(audioData));
  }, err => {
    console.error("ERROR DECODING:", err);
  });
});

const ws = fs.createWriteStream("rawAudio.raw");

const audioDevices = portAudio.getDevices();
console.log(audioDevices);


const ai = new portAudio.AudioInput(audioOptions);

ai.on("error", err => console.error(err));

ai.on("end", () => {
  console.log("ENDED", JSON.stringify(memStore));
});


ai.pipe(wstream);
//ai.pipe(ws);

//wstream.write(header(44100 * 8));
ws.write(header(44100 * 0));
ai.start();
setTimeout(() => {
  ai.quit();
  onBuffer(memStore.audio);
}, 5000);




function clearConsole() {
  process.stdout.write("\u001B[2J\u001B[0;0f");
}

const bpm = 118;

function getBpm () {
  const rand = Math.random();
  return rand < 0.3 ? 0.5 * bpm : rand > 0.8 ? 3 * bpm : rand > 0.7 ? 2 * bpm : bpm;
}


function runLights() {
  scan.on("light", async light => {
    try {
      const transition = 0;
      // 0 - 360
      const hue = 240;
      // 0 - 100
      const saturation = 100;
      // 0 - 100
      const brightness = 100;
      // 0 - 1000
      const color_temp = 0;
      console.info("light", JSON.stringify(light));

      const bulb = new TPLSmartDevice(light.ip);

      hsb(bulb, hue);

      /*
      setInterval(() => {
        hsb(bulb, Math.floor(Math.random()*360), 100, Math.floor(Math.random() * 100));
      }, 1000 * 60 / getBpm());
      */

    } catch (err) {
      console.error(err);
    }
  });
}


// 60 bpm = 1 bps = (1/1000 bpms)
// 100 bpm = 5/3 bps = 
async function rainbow(bulb, startHue, endHue) {
  if (startHue <= endHue) {
    while(startHue <= endHue) {
      await hsb(bulb, startHue);
      startHue++;
    }
  } else {
    while (startHue > endHue) {
      await hsb(bulb, startHue);
      startHue--;
    }
  }
}

async function hsb(bulb, hue, saturation = 100, brightness = 100, { color_temp = 0, transition = 100 } = {}) {
  const result = await bulb.power(true, transition, { color_temp, hue, saturation, brightness });
  console.info("hsb", jc(JSON.stringify(result, null, 2)));
}
